Design of a Sterile Insect Technique package
================

Requirements and design of a R package for analysing data from Mark-Capture-Recapture experiments for the effective management of invasive _Aedes_ mosquitoes.

The _Sterile Insect Technique_ (SIT) is being developed by the _Insect Pest Control Laboratory_ (IPCL) of the Joint FAO/IAEA _Centre of Nuclear Techniques in Food and Agriculture_ in Vienna, Austria.

### Documents

- [Project plan](https://umr-astre.pages.mia.inra.fr/sit-design/devtime_estimates.html)

- [Analysis of requirements](https://umr-astre.pages.mia.inra.fr/sit-design/requirements.html)

- [Project deliverable](https://umr-astre.pages.mia.inra.fr/sit-design/deliverable.html)


### Products

- [Package development repository](https://forgemia.inra.fr/umr-astre/sit)

- [Package web site](https://umr-astre.pages.mia.inra.fr/sit)
