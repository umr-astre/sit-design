---
title: Sterile Insect Technique 
author: Facundo Muñoz
date: "`r format(Sys.Date(), '%b, %Y')`"
output:
  pdf_document: default
  html_document: default
documentclass: cirad
---



## Context

2021-09-03 W Mamai

Sends by e-mail the file `mrr_alb_coord.csv` which replaces `mrr_alb_coord.xlsx`. It contains trap coordinates (not rounded, as it was the case) with an additional line of coordinates for the release point.
The other columns (`Habitat`, `Comment`, `Elevation`) were removed and replaced by `area`, which take values from either _private_, _public_ or _release point_.

He sends yet another file `trap_points.csv` with coordinates for the traps(this time seemingly in decimal degrees).


2021-08-26 W Mamai, J Bouyer, H Maiga

Discussed the preliminary version of the _Requirement Analysis_ document, and worked out several pending questions.

Mamai sent (the next day) by e-mail the control-area ovitrap data from the Albanian case study (`data/Albania/Ovitrap_Control area_during releases.xls`) and the corresponding protocol with additional information about the design that was missing in the data files (`doc/FIELD PROTOCOL MRR TIRANA FINAL_FB_FS_DA_Kela.doc`).


2021-05-20 W Mamai, J Bouyer, V Grosbois

Mamai Wadaka from Insect Pest Control Laboratory (IPCL) of the Joint FAO/IAEA Centre of Nuclear Techniques in Food and Agriculture in Vienna, Austria

IPCL develops the Sterile Insect Technique (SIT)

Training program "organise and analyse data from Mark-Release-Recapture" experiments in the frame of the technical cooperation project RER5026 _Enhancing the Capacity to Integrate Sterile Insect Technique in the Effective Management of Invasive Aedes Mosquitoes_

Proposition: develop a R-package for analysing the experimental results.

doc: `MRR_template.xls` contains experimental data and a full analysis performed in Excel.
This template was tailored for the MRR experiment performed in Cuba.
It is not completely generic. For instance, it assumes that there have been 4
releases from a single point, which is not always the case.
